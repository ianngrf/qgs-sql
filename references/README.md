# Referências

Site Oficial do SQLITE:

* [https://www.sqlite.org](https://www.sqlite.org)

Site Oficial SPATIALITE:

* [https://www.gaia-gis.it/fossil/libspatialite/index](https://www.gaia-gis.it/fossil/libspatialite/index)

Site Oficial QGIS:

* [Qgis](www.qgis.org)

Tutorial sobre SQL (w3schools):

*[https://www.w3schools.com/sql/default.asp](https://www.w3schools.com/sql/default.asp)

Tutorial sobre SQLITE:  

* [https://www.sqlitetutorial.net/](https://www.sqlitetutorial.net/)

Tutorial sobre Qgis:

* [https://kylefelipe.gitlab.io/apostila_qgis_3/](https://kylefelipe.gitlab.io/apostila_qgis_3/)

Vídeos do [Geocast Brasil](https://www.youtube.com/c/geocastbrasil) sobre SQL:

* [5 Coisas que precisamos saber sobre o POSTGRESQL](https://youtu.be/dsHwJBi4a2g)

* [Sugestões de livros: Banco de dados Geográficos e SQL](https://youtu.be/SNmrhW4g7sY)

Vídeos do [Luiz Sadeck](https://www.youtube.com/c/luissadeck) sobre SQL:

* [PostgreSQL & PostGIS](https://youtube.com/playlist?list=PLNFvG6bTA4NQ14Pkeim6YTCK0TSzxJEbq)

Juracy Almeida:

* [DML, DDL, DQL](http://www.juracyalmeida.com/o-que-e-dml-ddl-dcl-dql-no-sql/)

Diferença entre DECIMAL e FLOATING (Stackoverflow):

* https://pt.stackoverflow.com/questions/219211/qual-a-forma-correta-de-usar-os-tipos-float-double-e-decimal
