# SQL - Básico

Como toda linguagem, precisamos saber suas palavras reservadas e como utilizá-las, mas antes vamos ver três características do SQL para podermos ir entendendo algumas coisas.

A primeira delas é a `DML` - Data Manipulation Language.  
As expressões usadas aqui tem o intuito para manipular o dado do banco de dados (inserir dados, atualizar, remover).

- `INSERT` - Usado para inserir dados no banco.  
- `UPDATE` - Usado para atualizar um dado no banco.  
- `DELETE` - Usado para remover um dado do banco.

A `DDL` - Data Definition Language.  
Aqui são expressões utilizadas para definir os dados no banco de dados, quando criamos, alteramos ou removemos uma tabela do banco.

- `CREATE` - Usado para criar tabelas e outros objetos no banco.  
- `ALTER` - Usado para alterar tabelas e outros objetos no banco.  
- `DROP` - Usado para remover tabelas e outros objetos no banco.

A `DQL` - Data Query Language.  
A expressão aqui (repare o singular) é utilizada para fazer a consulta e retornar os dados do banco, fazer a SELEÇÃO do que queremos.

- `SELECT` - Usado para selecionar os dados no banco.

Essas expressões podem ser utilizadas em conjunto, independente de sua característica.

Podem ver mais sobre essas características do SQL [aqui](http://www.juracyalmeida.com/o-que-e-dml-ddl-dcl-dql-no-sql/).

Vamos usar essas expressões nos capítulos a seguir, e ficará mais fácil entender sua utilização.

Comentários no código SQL são feitos utilizando `-- ` no código que irá durar até o final da linha.

## Boas Práticas

Sim, é sempre bom termos boas práticas em tudo o que fazemos... e no SQL não seria diferente.

- __`KEYWORDS`__(expressões/palavras chaves) sempre são em CAIXA ALTA, para facilitar a leitura.  
  Exemplo:

  ```sql
  SELECT "campo" FROM "tabela";
  ```

- Nomes: Evite usar:  
  letras maiúsculas - Para não confundir com palavras chaves;  
  iniciar com números - Nomes que iniciam com números costumam causar problemas ao serem utilizados no SQLite, se for nome de arquivo, pode utilizar sem problemas;  
  siglas;
  NÃO UTILIZE: Caracteres especiais, espaços e acentuações, esses geram MUITOS problemas na utilização, quebram scripts e processos;  
Essas regras de nomes valem para, nomes de arquivos, pastas, funções, variáveis, tabelas e campos de tabelas.

- Aspas simples: Usadas quando estamos utilizando um valor em string.  
  Exemplo: `nome = 'Majimboo'`.

- Aspas duplas: Usadas em objetos do banco, como nomes de tabelas e campos.  
  Exemplos: `"nome" = 'Majimboo'`.

- Tipo de Dado: Texto é texto, número é número, lembre-se, é importante definirmos corretamente o tipo de dado, pois fazer operações com os dados ficam mais morosas quando temos de converter o dados para o tipo correto, por exemplo, números armazenados como texto, para serem trabalhados como números, precisam serem convertidos para serem utilizados.

Podemos ver mais sobre boas práticas nessa palestra [aqui](https://gitlab.com/kylefelipe/palestra_cbgeo_2019).

Qualidade de dados é um tema MUITO bom para se abordar sempre. E nunca fica velho, principalmente pelo motivo de sempre ter alguém que ignora essas regrinhas, e acaba tendo problemas futuros.

## Primeiros Passos

Bom, vamos começar a criar nossos scripts e começar a aprofundar na linguagem, e a partir de agora vamos ver _keywords_ que estão presentes em praticamentes TODOS OS BANCOS, quando formos utilizar alguma que for específica do banco, irei avisar.

Abra o [Qgis](www.qgis.org), adicione uma camada nele, agora vá no gerenciador de banco de dados, no painel `Provedores` vá em `Camada virtual > Camadas do projeto` e clique na camada que adicionou ao [Qgis](www.qgis.org) (__1__), isso irá estabelecer uma conexão entre a gerenciador e o provedor, permitindo utilizar QUALQUER camada que esteja dentro do provedor na consulta, não apenas a que foi clicada.  
Em seguida, clique em __\<Janela SQL>__ (__2__) para que uma nova aba apareça para digitarmos a consulta (__3__).
Após digitar a consulta, clique em __\<Executar>__  (__4__) para o gerenciador rodar o SQL e retornar o dado pesquisado.

LEMBRE-SE: [Qgis](www.qgis.org) usa a sintaxe do [SQLITE](https://www.sqlite.org) nas consultas.

![Conectando a um provedor](../img/0011_conectando_camadas_virtuais.png)

 e vamos ver o SQL funcionando na prática.

### SELECT

Vamos iniciar pela forma mais fácil que é usar o `SELECT`.  
Essa _keyword_ nos permite fazer seleção e dados no banco, podendo ser dados de uma ou várias tabelas ao mesmo tempo.

Possui uma sintáxe básica para sua utilização:

```sql
SELECT _<campos>_ -- Aqui indicamos os campos que queremos buscar e outras ações  
FROM _<tabela>_ -- Aqui informamos a tabela onde vamos buscar os dados  
WHERE _<filtro>_; -- Aplica um filtro  
```

Os campos determinados no `SELECT` podem ser indicados separados por vírgula, e as tabelas no `FROM` também.

O `WHERE` é opcional, não precisa ser sempre especificado no script, pois ele é utilizado para aplicar um filtro na seleção.

Podemos fazer seleção da seguinte forma no banco também:

```sql
SELECT _<tabela>_._<campo1>_, _<tabela>_._<campo2>_ --(...)
FROM _<tabela>_;
```

Essa forma é bem utilizada principalmente quando temos mais de uma tabela para buscar dados, assim, podemos especificar o nome do campo, principalmente se ambas as tabelas possuem nomes de campos iguais.

ATENÇÃO: procure sempre terminar o script SQL com um `;`.

### Definindo apelidos (ALIAS) - AS

Em algumas situações, podemos ter nomes de tabelas, campos muito grandes, ou nem um pouco expressivos, e para contornar esses problemas podemos utilizar o `AS`.

```sql
SELECT nome AS nome_estado, luf.sigla AS sigla_estado
FROM lim_unidade_federacao_a AS luf
WHERE sigla_estado = 'MG';
```

![Usando o SELECT](../img/0010_usando_select.png)

Viram que os nomes dos campos nome e sigla ficaram mais expressivos?  
Viram que o nome da tabela é muito grande?  
Usando o `AS` podemos definir outras denominações para os objetos e utilizar essas denominações no código.

### O coringa

Uma das consultas mais realizadas em SQL é aquela que retorna todos campos, seja de uma das tabelas da consulta ou de todas as tabelas, e para nos ajudar temos o coringa `*`.

Adicione agora a camada de municípios ao projeto (`lim_municipio_a`), para ela aparecer na lista, selecione `Camadas do Projeto` no painel de provedores, clique em __\<Atualizar>__ no alto da tela.

Rode o seguinte SQL na mesa aba.

```sql
SELECT *
FROM lim_municipio_a AS lim;
```

![Usando o coringa](../img/0012_coringa.png)

Viram que ele retorna todas as colunas da tabela, também podemos fazer da seguinte forma.

```sql
SELECT lim.*
FROM lim_municipio_a AS lim;
```

Essa forma é utilizada principalmente para pegar todos os campos de algumas das tabelas que estão sendo consultadas, basta colocar o nome/alias/apelido da tabela antes do coringa, seguido de um ponto.

### Limitando retorno da consulta

Viram que a operação anterior demorou mais tempo para retornar o resultado que a primeira que rodamos?  
Isso é devido à quantidade de dados que buscamos, o Brasil, atualmente, possui 5570 municípios, e a tabela possui 13 campos (12 tabulares e um de geometria).

![Informações da Tabela](../img/0013_informacoes_tabela.png)

E quanto mais dados temos de trabalhar, e quanto mais complexo é o processamento essa resposta pode levar muito tempo para ser retornada, principalmente quando estamos usando um hardware que não tem uma boa performace.

Em casos desse tipo, quando queremos testar nossos resultados, podemos limitar a quantidade de dados que recebemos de volta mesmo se estivermos usando algum filtro, com usamos na primeira consulta que fizermos.

Para isso usamos o `LIMIT <quantidade>`.

Ele irá _limitar_ a quantidade de linhas que vamos receber como resposta.

```sql
SELECT lim.*
FROM lim_municipio_a AS lim
LIMIT 1;
```

![Usando o limit](../img/0014_usando_limit.png)

Veja que agora essa consulta foi muito mais rápida.

E ao utilizarmos essa expressão ela deve vir SEMPRE POR ULTIMO na consulta.

### Ordenando a consulta

Podemos ordenar o nosso dado quando estamos trabalhando ele, seja em ordem de data/hora, alfabética ou numérica e etc.

Conseguimos esse efeito usando a expressão `ORDER BY` usando uma forma de comparação e especificando a ordem que queremos. Por padrão, o `ORDER BY` sempre ordena de forma crescente.

```sql
SELECT *
FROM lim_municipio_a AS lim
ORDER BY lim.nome
LIMIT 5;
```

![Ordenando os dados](../img/0015_ordenando_dados.png)

Podemos o sentido da ordenação usando `ASC` para crescente ou `DESC` para decrescentes.

```sql
SELECT *
FROM lim_municipio_a AS lim
ORDER BY lim.nome DESC
LIMIT 5;
```

![Mudando sentido da ordenação](../img/0016_sentido_ordem.png)

Também é possível usar campos de outros tipos, e até mais de um campo ao mesmo tempo para fazer a ordenação.  
Mas descobrir como fazer essa parte será seu dever de casa.

### DISTINÇÃO de valores

Em outros momentos queremos saber os valores únicos presentes na nossa tabela, quantas vezes ele aparece, utilizando o SQL podemos fazer isso facilmente usando o `DISTINCT()`.

Mas ele tem uma peculiaridade, precisa de ter parâmetro para fazer a distinção, esse parametro pode ser tanto um campo como uma expressão. Vamos fazer a forma mais simples que é passar um campo para fazer a distinção.

```sql
SELECT DISTINCT(anodereferencia)
FROM lim_municipio_a AS lim;
```

![Fazendo distinção de dados](../img/0017_distincao_dados.png)

Veja só sua sintaxe, ele vai direto no `SELECT` e deve ser uma das primeiras expressões que aparecem lá e passamos um campo.  
Ele avalia quais são os valores únicos que estão dentro desse campo e os retorna.  

### Contado valores

Podemos fazer contagens de dados também dentro do nosso script usando a expressão `COUNT()`, que também precisa de um parâmetro para a contagem.

```sql
SELECT COUNT(nome)
FROM lim_municipio_a AS lim;
```

![Usando o COUNT](../img/0018_contanto_dados.png)

Podemos contar todos os registros de outra forma também:

```sql
SELECT COUNT(1)
FROM lim_municipio_a AS lim;
```

![Outra forma de contar dados](../img/0019_contanto_dados_1.png)

Assim o sql irá também contar todos os dados dentro da tabela.

Lembra que, quando usamos o `DISTINCT` no item anterior, o `anodereferencia` tinha um dado nulo?, veja o que acontece ao contarmos os dados nesse campo.

```sql
SELECT COUNT(anodereferencia)
FROM lim_municipio_a AS lim;
```

Por isso o `COUNT(1)` é interessante de ser usado, mesmo que haja um valor nulo, ele será contabilizado.

Podemos usar outras expressões em conjunto, veja esse exemplo usando o DISTINCT() junto com o COUNT().

```sql
SELECT DISTINCT(anodereferencia) AS dado_contado, COUNT(anodereferencia) AS quantidade
FROM lim_municipio_a AS lim;
```

![Contando distinções](../img/0020_contando_distinguindo.png)

No SQL podemos mesclar muitas expressões para conseguirmos nossos resultados.

### Agrupando valores

Uma outra forma de fazer distinção de valores no SQL é fazendo o agrupamento desses valores, podemos usar um campo ou uma expressão mais elaborada para isso.

```sql
SELECT anodereferencia
FROM lim_municipio_a AS lim
GROUP BY anodereferencia
ORDER BY anodereferencia;
```

![Agrupando valores](../img/0021_agrupando_valores.png)

Mas qual o motivo de termos o `GROUP BY` se temos o `DISTINC`??  
Simples, o `GROUP BY` indica que há valores nulos, mas eles não são contados.

```sql
SELECT anodereferencia, COUNT(1)
FROM lim_municipio_a AS lim
GROUP BY anodereferencia;
```

![Contando e agrupando](../img/0022_agrupando_contando.png)

Repare, o `ORDER BY` vindo no final e no sentido decrescente.

### Concatenando campos

Fazer concatenação de campos é um processo bem conhecido de quem já trabalha com o [Qgis](www.qgis.org) e precisa fazer rótulos e outras coisas.

```sql
SELECT nome, geocodigo, nome || ' - ' || geocodigo  AS nome_geocodigo
FROM  lim_municipio_a AS lim
ORDER BY nome
LIMIT 5;
```

![Concatenando campos](../img/0023_concatenando_campos.png)

O importante é ter em mente que algumas coisas precisam de seguir alguma ordem...

```sql
SELECT
FROM
WHERE
GROUP BY
ORDER BY
LIMIT
; 
```

Grande mestre [Narcélio](http://www.narceliodesa.com) uma vêz fez uma grande observação quanto ao SQL em uma de nossas conversas:

> O legal o SQL é que se pegarmos nossa questão e traduzirmos para o inglês, temos ele praticamente pronto!
> Por exemplo:
> Selecione o nome dos municípios em ordem alfabética pelo nome;
> traduzindo ficaria:
> SELECT nome FROM municípios ORDERED BY nome;
> É isso é muito lindo.

A diferença da frase traduzida para o SQL foi o tempo verbal de ORDEM, que no SQL ficaria no infinitivo (`ORDER`).
